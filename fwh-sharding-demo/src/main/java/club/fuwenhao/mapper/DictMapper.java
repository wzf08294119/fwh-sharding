package club.fuwenhao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import club.fuwenhao.entity.Dict;

/**
 * @author ：楼兰
 * @date ：Created in 2021/1/5
 * @description:
 **/
public interface DictMapper extends BaseMapper<Dict> {
}
