package club.fuwenhao.spidemo;

/**
 * @program: fwh-sharding
 * @description: 动物接口
 * @author: fwh
 * @date: 2021-11-30 13:55
 **/
public interface Animal {
    /**
     * 声音
     */
    void sound();
}
